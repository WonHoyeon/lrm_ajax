package com.lrmStart.dao;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.lrmStart.model.V22InsertVO;


@Repository("V22DAO")
public class V22DAOImpl implements V22DAO {
	
	@Inject
	private SqlSession sqlSession;

	@Override
	public List<Map<String, Object>> v22Select(String name) throws Exception {
		return sqlSession.selectList("v22Mapper.v22Select", name);
	}

	@Override
	public void v22Insert(V22InsertVO v22vo) throws Exception {
		sqlSession.insert("v22Mapper.v22Insert", v22vo);
	}

	@Override
	public void v22Delete(V22InsertVO v22vo) throws Exception {
		sqlSession.update("v22Mapper.v22Delete", v22vo);
	}

}