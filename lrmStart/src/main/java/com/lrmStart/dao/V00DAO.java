package com.lrmStart.dao;

import java.util.List;
import java.util.Map;


import com.lrmStart.model.V00InsertVO;



public interface V00DAO {

	public List<Map<String, Object>> v00Select(String hakbun) throws Exception;
	
	public Map<String, Object> stuSelect(String hakbun) throws Exception;
	
	
	public void v00Insert(V00InsertVO v00vo) throws Exception;
	
	public int isADM(String id, String pw) throws Exception;
	
	public void v00Delete(V00InsertVO v00vo) throws Exception;
}