package com.lrmStart.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.lrmStart.model.V11InsertModel;
import com.lrmStart.model.V11InsertVO;


@Repository("V11DAO")
public class V11DAOImpl implements V11DAO {
	
	@Inject
	private SqlSession sqlSession;

	@Override
	public List<Map<String, Object>> v11Select(String code, String begDate, String endDate) throws Exception {
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("code", code);
		paramMap.put("begDate", begDate);
		paramMap.put("endDate", endDate);
		
		return sqlSession.selectList("v11Mapper.v11Select", paramMap);
	}


	@Override
	public void v11Insert(V11InsertVO v11vo) throws Exception {
		sqlSession.insert("v11Mapper.v11Insert", v11vo);
	}


	@Override
	public void v11Delete(V11InsertVO v11vo) throws Exception {
		sqlSession.update("v11Mapper.v11Delete", v11vo);
	}


//	@Override
//	public int remove(String no) throws Exception {
//		return sqlSession.update("v11Mapper.remove", no);
//	}
	@Override
	public int remove(V11InsertModel v11mo) throws Exception {
		return sqlSession.update("v11Mapper.remove2", v11mo);
	}

}