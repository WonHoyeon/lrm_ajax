package com.lrmStart.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.lrmStart.model.V33VO;


@Repository("V33DAO")
public class V33DAOImpl implements V33DAO {
	
	@Inject
	private SqlSession sqlSession;

	@Override
	public List<Map<String, Object>> v33Select(String begDate, String endDate) throws Exception {
		
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("begDate", begDate);
		paramMap.put("endDate", endDate);
		return sqlSession.selectList("v33Mapper.v33Select", paramMap);
	}

	@Override
	public void v33Modify(V33VO v33vo) throws Exception {
		sqlSession.update("v33Mapper.v33Modify", v33vo);
	}

	@Override
	public void v33CrData() throws Exception {
		sqlSession.insert("v33Mapper.v33CrData");
	}
	@Override
	public void v33DelData() throws Exception {
		sqlSession.insert("v33Mapper.v33DelData");
	}
	@Override
	public void v33Delete(V33VO v33vo) throws Exception {
		sqlSession.update("v33Mapper.v33Delete", v33vo);
	}

	@Override
	public void v33AllFix(V33VO v33vo) throws Exception {
		sqlSession.update("v33Mapper.v33AllFix", v33vo);
	}

}