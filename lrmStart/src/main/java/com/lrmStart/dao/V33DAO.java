package com.lrmStart.dao;

import java.util.List;
import java.util.Map;


import com.lrmStart.model.V33VO;



public interface V33DAO {

	public List<Map<String, Object>> v33Select(String begDate, String endDate) throws Exception;
	
	public void v33CrData() throws Exception;
	
	public void v33DelData() throws Exception;
	
	public void v33Modify(V33VO v33vo) throws Exception;
	
	public void v33Delete(V33VO v33vo) throws Exception;
	
	public void v33AllFix(V33VO v33vo) throws Exception;
}