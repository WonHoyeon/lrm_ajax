package com.lrmStart.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.lrmStart.model.V00InsertVO;


@Repository("V00DAO")
public class V00DAOImpl implements V00DAO {
	
	@Inject
	private SqlSession sqlSession;

	@Override
	public List<Map<String, Object>> v00Select(String hakbun) throws Exception {
		return sqlSession.selectList("v00Mapper.v00Select", hakbun);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> stuSelect(String hakbun) throws Exception {
		
		return (Map<String, Object>) sqlSession.selectOne("v00Mapper.studentInfo", hakbun);
	}


	@Override
	public void v00Insert(V00InsertVO v00vo) throws Exception {
		sqlSession.insert("v00Mapper.v00Insert", v00vo);
	}

	@Override
	public int isADM(String id, String pw) throws Exception {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("id", id);
		paramMap.put("pw", pw);
		
		return sqlSession.selectOne("v00Mapper.userCheck", paramMap);
	}

	@Override
	public void v00Delete(V00InsertVO v00vo) throws Exception {
		sqlSession.update("v00Mapper.v00Delete", v00vo);
	}

}