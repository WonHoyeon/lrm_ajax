package com.lrmStart.dao;

import java.util.List;
import java.util.Map;

import com.lrmStart.model.V22InsertVO;



public interface V22DAO {

	public List<Map<String, Object>> v22Select(String name) throws Exception;
	
	public void v22Insert(V22InsertVO v22vo) throws Exception;
	
	public void v22Delete(V22InsertVO v22vo) throws Exception;
}