package com.lrmStart.dao;



import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.lrmStart.model.V44VO;


@Repository("V44DAO")
public class V44DAOImpl implements V44DAO {
	
	@Inject
	private SqlSession sqlSession;

	
	@Override
	public void v44ListPrint(V44VO v44vo) throws Exception {
		sqlSession.insert("v44Mapper.v44Print", v44vo);
	}

}