package com.lrmStart.model;

import java.sql.Date;

public class V22InsertVO {

	private Date reservation;
	private String region;
	private String name;
	private String htelno;
	private String bigo;
	private String no;
	
	
	public Date getReservation() {
		return reservation;
	}
	public void setReservation(Date reservation) {
		this.reservation = reservation;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHtelno() {
		return htelno;
	}
	public void setHtelno(String htelno) {
		this.htelno = htelno;
	}
	public String getBigo() {
		return bigo;
	}
	public void setBigo(String bigo) {
		this.bigo = bigo;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}

	
	
	
}
