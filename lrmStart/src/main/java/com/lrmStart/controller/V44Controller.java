package com.lrmStart.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lrmStart.service.V00Service;
import com.lrmStart.service.V44Service;

@Controller
public class V44Controller {

//	V00Controller httpSession = new V00Controller();

	@Resource(name = "V00Service")
	private V00Service v00service;
	
	@Resource(name = "V44Service")
	private V44Service v44service;

//	private static final Logger Log = LoggerFactory.getLogger(VController.class);

	@RequestMapping("/V44Print")
	public String go1(Model model, HttpSession session) throws Exception {
		if(v00service.isADM(session) == 1) {
		return "V44Print";
		}else {
			return "login";
		}
	}
	@RequestMapping(value = "/V44ListPrint", method = RequestMethod.GET)
	public ModelAndView go2(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		System.out.println(request.getParameter("code"));
		System.out.println(request.getParameter("fix"));
		System.out.println(request.getParameter("reservation2"));
		
		ModelAndView mav = new ModelAndView();
		if(v00service.isADM(session) == 1) {
		String reservation2 = request.getParameter("reservation2");
		String begDate = reservation2.split(" - ")[0].replace("-","");
		String endDate = reservation2.split(" - ")[1].replace("-","");
		
		System.out.println(begDate);
		System.out.println(endDate);
		
		
		mav.addObject("report_name","lrm_p02");
    	mav.addObject("code",request.getParameter("code"));
    	mav.addObject("begDate", begDate);
    	mav.addObject("endDate", endDate);
    	mav.addObject("fix", request.getParameter("fix"));
		
		mav.setViewName("V44PrintViewer");
		
		
		}else {
		mav.setViewName("login");
		}
		return mav;
	}

	@RequestMapping(value = "/Save", method = { RequestMethod.GET, RequestMethod.POST })
	public String go3(HttpSession session) throws Exception {
		if(v00service.isADM(session) == 1) {
			
			
			
		}else {
			return "login";
		}

		return "main";
	}
//	@RequestMapping(value="/V11DeleteList", method = {RequestMethod.GET, RequestMethod.POST})
//	public String go4(@ModelAttribute V11InsertVO v11vo, HttpServletRequest request, RedirectAttributes rttr)  throws Exception {
//		
//		v11vo.setNO(request.getParameter("No"));
//		v11vo.setUSERID(request.getParameter("Id"));
//		v11vo.setUSERIP(request.getRemoteAddr());
//		v11service.v11Delete(v11vo);
//		 
//		 Map<String, Object> paramMap = new HashMap<>();
//		 paramMap.put("msg", "삭제되었습니다.");
//		
//		 rttr.addFlashAttribute("paramMap", paramMap);
//		 
//		return "redirect:/Redirect";
//	}
//	
//	
//	@RequestMapping(value="/Redirect", method = {RequestMethod.GET})
//	public ModelAndView testRedirect(@ModelAttribute(value="paramMap") Map<String, Object> paramMap, HttpSession httpSession){
//		
//		ModelAndView mav = new ModelAndView();
//
//		try {
//			mav.addObject("vInfo", v11service.v11Select((String)httpSession.getAttribute("hakbun")));
//		} catch (Exception e) {
//			System.out.println("리다이렉트 예외1");
//		}
//		try {
//			mav.addObject("stuInfo", v11service.stuSelect((String)httpSession.getAttribute("hakbun")));
//		} catch (Exception e) {
//			System.out.println("리다이렉트 예외2");
//		}
//		System.out.println("v11controller에서"+(String)httpSession.getAttribute("ID"));
//		System.out.println("v11controller에서"+(String)httpSession.getAttribute("hakbun"));
//		mav.addObject("hakbun", (String)httpSession.getAttribute("hakbun"));
//		mav.setViewName("V11List");
//		mav.addObject("msg", paramMap.get("msg"));
//		
//		return mav;
//	}

}