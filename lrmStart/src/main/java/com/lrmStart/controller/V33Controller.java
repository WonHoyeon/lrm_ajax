package com.lrmStart.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lrmStart.model.V33VO;
import com.lrmStart.service.V00Service;
import com.lrmStart.service.V33Service;

@Controller
public class V33Controller {
	
//	V00Controller httpSession = new V00Controller();

	@Resource(name = "V00Service")
	private V00Service v00service;
	
	@Resource(name = "V33Service")
	private V33Service v33service;

//	private static final Logger Log = LoggerFactory.getLogger(VController.class);
	
	
	@RequestMapping("/V33Check")
	public String go1(Model model, HttpSession session) throws Exception {
		if(v00service.isADM(session) == 1) {
		return "V33Check";
		}else {
			return "login";
		}
	}

	@RequestMapping(value="/V33Data", method = {RequestMethod.POST})
	public String go2(Model model, HttpServletRequest request, HttpSession session)  throws Exception {
		
//		Map<String, Object> map = model.asMap();
//		HttpServletRequest req = (HttpServletRequest) map.get("request");
		if(v00service.isADM(session) == 1) {
		String reservation2 = request.getParameter("reservation2");
		String begDate = reservation2.split(" - ")[0];
		String endDate = reservation2.split(" - ")[1];
		v33service.v33CrData();
		v33service.v33DelData();
		model.addAttribute("vInfo", v33service.v33Select(begDate, endDate));
		session.setAttribute("begDate", begDate);
		session.setAttribute("endDate", endDate);

		return "V33List";
		}else {
			return "login";
		}
	}
	

	@RequestMapping(value="/V33DeleteList", method = RequestMethod.POST)
	public String go3(@ModelAttribute V33VO v33vo, HttpServletRequest request, HttpSession session)  throws Exception {
		if(v00service.isADM(session) == 1) {
		v33vo.setNo(request.getParameter("no"));
		System.out.println(v33vo.getNo());
		v33service.v33Delete(v33vo);
		return "redirect:/v33";
		}else {
			return "login";
		}
	}
	
	@RequestMapping(value="/V33ModifyList", method = RequestMethod.POST)
	public String go4(@ModelAttribute V33VO v33vo, HttpServletRequest request, HttpSession session)  throws Exception {
		if(v00service.isADM(session) == 1) {		
		String reservation3 = request.getParameter("reservation3");
		String begDate = reservation3.split(" - ")[0];
		String endDate = reservation3.split(" - ")[1];
		
		v33vo.setUserid((String)session.getAttribute("id"));
		v33vo.setBegDate(begDate);
		v33vo.setEndDate(endDate);
		
		System.out.println(v33vo.getUserid());		
		System.out.println(v33vo.getNo());		
		System.out.println(v33vo.getBegDate());		
		System.out.println(v33vo.getEndDate());		
		System.out.println(v33vo.getFix());		
		System.out.println(v33vo.getPcnt());		
		System.out.println(v33vo.getPenalty());		
		v33service.v33Modify(v33vo);
		 
		return "redirect:/v33";
		}else {
			return "login";
		}
	}
	
	
	@RequestMapping(value="/v33", method = {RequestMethod.GET})
	public ModelAndView re(HttpSession session) throws Exception{
		ModelAndView mav = new ModelAndView();
		if(v00service.isADM(session) == 1) {
		String begDate = (String)session.getAttribute("begDate");
		String endDate = (String)session.getAttribute("endDate");

		mav.addObject("vInfo", v33service.v33Select(begDate, endDate));
		mav.setViewName("V33List");
		
		
		}else {
			mav.setViewName("login");
		}
		return mav;
	}
	
	@RequestMapping(value="/v33ListClick", method = {RequestMethod.GET})
	public String go5(Model model, HttpServletRequest request, HttpSession session)  throws Exception {
		
//		Map<String, Object> map = model.asMap();
//		HttpServletRequest req = (HttpServletRequest) map.get("request");
		if(v00service.isADM(session) == 1) {
		String reservation2 = request.getParameter("reservation2");
		String begDate = reservation2.split(" - ")[0];
		String endDate = reservation2.split(" - ")[1];
		model.addAttribute("vInfo", v33service.v33Select(begDate, endDate));
		session.setAttribute("begDate", begDate);
		session.setAttribute("endDate", endDate);

		return "V33List";
		}else {
			return "login";
		}
	}
	
	@RequestMapping(value="/v33AllFix", method = {RequestMethod.GET})
	public String go6(V33VO v33vo, HttpSession session)  throws Exception {
		
//		Map<String, Object> map = model.asMap();
//		HttpServletRequest req = (HttpServletRequest) map.get("request");
		if(v00service.isADM(session) == 1) {
		String begDate = (String)session.getAttribute("begDate");
		String endDate = (String)session.getAttribute("endDate");
		String id = (String)session.getAttribute("id");
		v33vo.setBegDate(begDate);
		v33vo.setEndDate(endDate);
		v33vo.setUserid(id);
		v33service.v33AllFix(v33vo);
		
		return "redirect:/v33";
		}else {
			return "login";
		}
	}

	


}