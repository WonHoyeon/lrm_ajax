package com.lrmStart.controller;



import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lrmStart.model.V00InsertVO;
import com.lrmStart.service.V00Service;

@Controller
public class V00Controller {
	
//	CommonController httpSession = new CommonController();

	@Resource(name = "V00Service")
	private V00Service v00service;

//	private static final Logger Log = LoggerFactory.getLogger(VController.class);
	
	
	@RequestMapping("/V00Check")
	public String go1(HttpSession session, HttpServletRequest request) throws Exception {
		if(v00service.isADM(session) == 1) {
		return "V00Check";
		}else {
			return "login";
		}
		
	}

	@RequestMapping(value="/V00ListClick", method = {RequestMethod.GET})
	public String go2(Model model, HttpServletRequest request, HttpSession session)  throws Exception {
		
//		Map<String, Object> map = model.asMap();
//		HttpServletRequest req = (HttpServletRequest) map.get("request");
		if(v00service.isADM(session) == 1) {
		String hakbun = request.getParameter("hakbun");
	
		model.addAttribute("vInfo", v00service.v00Select(hakbun));
		model.addAttribute("stuInfo", v00service.stuSelect(hakbun));
		model.addAttribute("hakbun", hakbun);
		session.setAttribute("hakbun", hakbun);

		return "V00List";
		
		}else {
			return "login";
		}
	}
	
	
	@RequestMapping(value="/V00InsertList", method = RequestMethod.GET)
	public String go3(@ModelAttribute V00InsertVO v00vo, HttpServletRequest request, HttpSession session)  throws Exception {

		if(v00service.isADM(session) == 1) {
		v00vo.setUserid((String)session.getAttribute("id"));
		v00vo.setHakbun((String)session.getAttribute("hakbun"));
		v00service.v00Insert(v00vo);
		
		return "redirect:/V00";
		}else {
			return "login";
		}
	}
	
	
	@RequestMapping(value="/V00DeleteList", method = RequestMethod.POST)
	public String go4(@ModelAttribute V00InsertVO v00vo, HttpServletRequest request, HttpSession session)  throws Exception {
		if(v00service.isADM(session) == 1) {
		v00vo.setUserid((String)session.getAttribute("id"));
		v00service.v00Delete(v00vo);
		 
		return "redirect:/V00";
		}else {
			return "login";
		}
	}
	
	
	@RequestMapping(value="/V00", method = {RequestMethod.GET})
	public ModelAndView re(HttpSession session) throws Exception{
		ModelAndView mav = new ModelAndView();
		if(v00service.isADM(session) == 1) {
		mav.addObject("vInfo", v00service.v00Select((String)session.getAttribute("hakbun")));
		mav.addObject("stuInfo", v00service.stuSelect((String)session.getAttribute("hakbun")));
	
		System.out.println("vcontroller����"+(String)session.getAttribute("id"));
		System.out.println("vcontroller����"+(String)session.getAttribute("hakbun"));
		mav.addObject("hakbun", (String)session.getAttribute("hakbun"));
		mav.setViewName("V00List");
		
		}else {
			mav.setViewName("login");
		}
		return mav;
	}
	
	



}