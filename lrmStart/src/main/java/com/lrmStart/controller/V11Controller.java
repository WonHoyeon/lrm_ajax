package com.lrmStart.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lrmStart.model.V11InsertVO;
import com.lrmStart.service.V00Service;
import com.lrmStart.service.V11Service;

@Controller
public class V11Controller {
	
//	V00Controller httpSession = new V00Controller();

	
	@Resource(name = "V00Service")
	private V00Service v00service;
	
	@Resource(name = "V11Service")
	private V11Service v11service;

//	private static final Logger Log = LoggerFactory.getLogger(VController.class);
	
	
	@RequestMapping("/V11Check")
	public String go1(Model model, HttpSession session, HttpServletRequest request) throws Exception {
		if(v00service.isADM(session) == 1) {
		System.out.println("!!!!!!!!!세션 테스트 !!!!!!"+ session.getAttribute("hakbun"));
		System.out.println("!!!!!!!!!세션 테스트 !!!!!!"+ session.getAttribute("begDate"));
		System.out.println("!!!!!!!!!세션 테스트 !!!!!!"+ session.getAttribute("endDate"));
		return "V11Check";
		}else {
			return "login";
		}
	}

	@RequestMapping(value="/V11ListClick", method = {RequestMethod.GET})
	public String go2(Model model, HttpServletRequest request, HttpSession session)  throws Exception {
		if(v00service.isADM(session) == 1) {
		String code = request.getParameter("code");
		String reservation2 = request.getParameter("reservation2");
		String begDate = reservation2.split(" - ")[0];
		String endDate = reservation2.split(" - ")[1];
		
		
		
		model.addAttribute("vInfo", v11service.v11Select(code, begDate, endDate));
		model.addAttribute("listInfo", code);
		model.addAttribute("code", code);
		session.setAttribute("code", code);
		session.setAttribute("begDate", begDate);
		session.setAttribute("endDate", endDate);

		return "V11List";
		}else {
			return "login";
		}
	}
	
	
	@RequestMapping(value="/V11InsertList", method = RequestMethod.GET)
	public String go3(@ModelAttribute V11InsertVO v11vo, HttpServletRequest request, HttpSession session)  throws Exception {
		if(v00service.isADM(session) == 1) {
		session.setAttribute("hakbun", request.getParameter("hakbun"));
		v11vo.setHakbun(request.getParameter("hakbun"));
		v11vo.setUserid((String)session.getAttribute("id"));
		v11vo.setCode((String)session.getAttribute("code"));
		v11service.v11Insert(v11vo);
		
		return "redirect:/V11";
		}else {
			return "login";
		}
	}
	
	@RequestMapping(value="/V11DeleteList", method = RequestMethod.POST)
	public String go4(@ModelAttribute V11InsertVO v11vo, HttpServletRequest request, HttpSession session)  throws Exception {
		if(v00service.isADM(session) == 1) {
		v11vo.setUserid((String)session.getAttribute("id"));
		v11service.v11Delete(v11vo);
		 
		return "redirect:/V11";
		}else {
			return "login";
		}
	}
	
	
	@RequestMapping(value="/V11", method = {RequestMethod.GET})
	public ModelAndView re(HttpSession session) throws Exception{
		ModelAndView mav = new ModelAndView();
		if(v00service.isADM(session) == 1) {
		String code = (String)session.getAttribute("code");
		String begDate = (String)session.getAttribute("begDate");
		String endDate = (String)session.getAttribute("endDate");

		
		mav.addObject("vInfo", v11service.v11Select(code, begDate, endDate));
		mav.addObject("listInfo", (String)session.getAttribute("code"));
		
		System.out.println("v11controller에서"+(String)session.getAttribute("id"));
		System.out.println("v11controller에서"+(String)session.getAttribute("hakbun"));
		mav.setViewName("V11List");
		
		
		}else {
			mav.setViewName("login");
		}
		return mav;
	}

	
	
	
	
	
	
	
	
	

//	@RequestMapping(value="/V11Redirect/{bno}",consumes="application/json",  method = {RequestMethod.GET})
//	public ModelAndView testRedirect(@ModelAttribute(value="paramMap") Map<String, Object> paramMap, HttpSession httpSession){
//	
//
//		ModelAndView mav = new ModelAndView();
//
//		try {
//			mav.addObject("vInfo", v11service.v11Select((String)httpSession.getAttribute("code"),
//														(String)httpSession.getAttribute("begDate"),
//														(String)httpSession.getAttribute("endDate")));
//		} catch (Exception e) {
//			System.out.println("리다이렉트 예외1");
//		}
//		try {
//			mav.addObject("listInfo", (String)httpSession.getAttribute("code"));
//		} catch (Exception e) {
//			System.out.println("리다이렉트 예외2");
//		}
//		System.out.println("v11controller에서"+(String)httpSession.getAttribute("ID"));
//		System.out.println("v11controller에서"+(String)httpSession.getAttribute("hakbun"));
//		mav.setViewName("V11List");
//		mav.addObject("msg", paramMap.get("msg"));
//		
//		
//		return mav;
//	}


//	@RequestMapping(value="/{no}", produces= {MediaType.TEXT_PLAIN_VALUE}, method = {RequestMethod.PUT})
//	public ResponseEntity<String> remove(@PathVariable("no") String no) throws Exception{
//		
//		System.out.println("testval" + no);
//
//		ResponseEntity testValue = v11service.remove(no) == 1? 
//				new ResponseEntity<>("success", HttpStatus.OK)
//						:new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//				
//				System.out.println(testValue);
//		
//		
//		
//			return testValue;
//	}
	
	
//	
//	@RequestMapping(value="/{no}/{userid}", consumes="application/json", produces= {MediaType.TEXT_PLAIN_VALUE}, method = {RequestMethod.PUT})
//	public ResponseEntity<String> remove(@RequestBody V11InsertModel v11mo) throws Exception{
//		
//		System.out.println("testval" + v11mo.getNo());
//		System.out.println("testval" + v11mo.getUserid());
//		v11mo.setNo(v11mo.getNo());
//		v11mo.setUserid(v11mo.getUserid());
//		
//		System.out.println("testval" + v11mo.getNo());
//		System.out.println("testval" + v11mo.getUserid());
//
//		ResponseEntity testValue = v11service.remove(v11mo) == 1? 
//				new ResponseEntity<>("success", HttpStatus.OK)
//		
//						:new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//				
//				
//				System.out.println(testValue);
//				
//			return testValue;
//	}
	
}