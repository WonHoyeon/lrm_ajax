package com.lrmStart.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lrmStart.model.V22InsertVO;
import com.lrmStart.service.V00Service;
import com.lrmStart.service.V22Service;

@Controller
public class V22Controller {
	
//	V00Controller httpSession = new V00Controller();

	@Resource(name = "V00Service")
	private V00Service v00service;
	
	@Resource(name = "V22Service")
	private V22Service v22service;

//	private static final Logger Log = LoggerFactory.getLogger(VController.class);
	
	
	@RequestMapping("/V22Check")
	public String go1(Model model, HttpSession session) throws Exception {
		if(v00service.isADM(session) == 1) {
		return "V22Check";
		}else {
			return "login";
		}
	}

	@RequestMapping(value="/V22ListClick", method = {RequestMethod.GET})
	public String go2(Model model, HttpServletRequest request, HttpSession session)  throws Exception {
		if(v00service.isADM(session) == 1) {
		String name = request.getParameter("name");
	
		model.addAttribute("vInfo", v22service.v22Select(name));
		session.setAttribute("name", name);

		return "V22List";
		}else {
			return "login";
		}
	}
	
	
	@RequestMapping(value="/V22InsertList", method = RequestMethod.GET)
	public String go3(@ModelAttribute V22InsertVO v22vo, HttpServletRequest request, HttpSession session)  throws Exception {
		if(v00service.isADM(session) == 1) {
		v22service.v22Insert(v22vo);
		
		return "redirect:/v22";
		}else {
			return "login";
		}
	}
	
	@RequestMapping(value="/V22DeleteList", method = RequestMethod.POST)
	public String go4(@ModelAttribute V22InsertVO v22vo, HttpServletRequest request, HttpSession session)  throws Exception {
		if(v00service.isADM(session) == 1) {
		v22service.v22Delete(v22vo);
		
		return "redirect:/v22";
		}else {
			return "login";
		}
	}
	
	
	@RequestMapping(value="/v22", method = {RequestMethod.GET})
	public ModelAndView re(HttpSession session) throws Exception{
		
		ModelAndView mav = new ModelAndView();
		
		if(v00service.isADM(session) == 1) {
		mav.addObject("vInfo", v22service.v22Select((String)session.getAttribute("name")));
		mav.addObject("name", (String)session.getAttribute("name"));
		mav.setViewName("V22List");
		
		System.out.println("v22controller¿¡¼­"+(String)session.getAttribute("name"));
		
		
		}else {
			mav.setViewName("login");
		}
		return mav;
	}
	
	


}