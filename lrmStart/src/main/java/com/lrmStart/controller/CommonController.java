package com.lrmStart.controller;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.lrmStart.service.V00Service;

@Controller
public class CommonController {
	

//	public HttpSession httpSession = null;

	@Resource(name = "V00Service")
	private V00Service v00service;
	
	
	@RequestMapping(value = "/", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView login(HttpSession session) {
		ModelAndView mav = new ModelAndView();
		if((String)session.getAttribute("ID") == null) {
			mav.setViewName("login");
			
		}else {
			mav.setViewName("main");
		}
		return mav;
	}	
	
	@RequestMapping(value = "/main", method = {RequestMethod.POST})
	public ModelAndView chk(@RequestParam("userID") String id, @RequestParam("userPWD") String pw, HttpSession session, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		
		id = id.toUpperCase();
		System.out.println("유저체크 1");
		session.setAttribute("id", id);
		session.setAttribute("pw", pw);
		System.out.println("유저체크 2");
		try {
			if(v00service.isADM(session) == 1) {
				mav.setViewName("main");
//				session.setAttribute("admChk", 1);
				@SuppressWarnings("unused")
				Cookie cookie = new Cookie("admChk", "1");
				response.addCookie(cookie);
				cookie.setMaxAge(-1);
				
			}
			else {
				session.removeAttribute("id");
				session.removeAttribute("pw");
				mav.setViewName("login");
				mav.addObject("msg","관리자가 아닙니다.");
			}
		} catch (Exception e) {
			System.out.println("예외발생");
		}
		System.out.println("유저체크 3");
		System.out.println("유저체크후" + (String)session.getAttribute("id"));
		return mav;
	}

	
	@RequestMapping(value = "main", method = {RequestMethod.GET})
	public String index(HttpSession session) throws Exception {
		
		if(v00service.isADM(session) == 1) {
		System.out.println("로고 누르거나 /main 입력후 메인페이지 접속할때 ");
		System.out.println("vcommon에서"+(String)session.getAttribute("id"));
		System.out.println("vcommon에서"+(String)session.getAttribute("hakbun"));
		return "main";
	} else {
		return "redirect:/";
	}
	}
	
	@RequestMapping("/logout")
	public String logout(HttpSession session){
		System.out.println("로그아웃");
		session.invalidate();
//		session.removeAttribute("id"); 
//		session.removeAttribute("pw"); 
	   return "redirect:/";
	}
}
