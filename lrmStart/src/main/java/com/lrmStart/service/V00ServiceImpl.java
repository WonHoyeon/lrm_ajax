package com.lrmStart.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.lrmStart.dao.V00DAO;
import com.lrmStart.model.V00InsertVO;


@Service("V00Service")
public class V00ServiceImpl implements V00Service {

	    @Resource(name="V00DAO")
	    private V00DAO v00dao;
	
	@Override
	public List<Map<String, Object>> v00Select(String hakbun) throws Exception {
		return v00dao.v00Select(hakbun);
	}
	@Override
	public Map<String, Object> stuSelect(String hakbun) throws Exception {
		return v00dao.stuSelect(hakbun);
	}
	@Override
	public void v00Insert(V00InsertVO v00vo) throws Exception {
		v00dao.v00Insert(v00vo);
	}
	@Override
	public int isADM(HttpSession session) throws Exception {
		String id = (String)session.getAttribute("id");
		String pw = (String)session.getAttribute("pw");
		if(id != null && pw != null) {
		
		return v00dao.isADM(id, pw);
		}else {return 0;}
	}
	
	@Override
	public void v00Delete(V00InsertVO v00vo) throws Exception {
		v00dao.v00Delete(v00vo);
	}
}
