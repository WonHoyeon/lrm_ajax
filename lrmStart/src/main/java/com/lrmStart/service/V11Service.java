package com.lrmStart.service;

import java.util.List;
import java.util.Map;

import com.lrmStart.model.V11InsertModel;
import com.lrmStart.model.V11InsertVO;

public interface V11Service {
	
	public List<Map<String, Object>> v11Select(String code, String begDate, String endDate) throws Exception;
	
	public void v11Insert(V11InsertVO v11vo) throws Exception;
	
	public void v11Delete(V11InsertVO v11vo) throws Exception;
	
	public int remove(V11InsertModel v11mo) throws Exception;
	

}