package com.lrmStart.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lrmStart.dao.V22DAO;
import com.lrmStart.model.V22InsertVO;


@Service("V22Service")
public class V22ServiceImpl implements V22Service {

	    @Resource(name="V22DAO")
	    private V22DAO v22dao;
	
	@Override
	public List<Map<String, Object>> v22Select(String name) throws Exception {
		return v22dao.v22Select(name);
	}
	@Override
	public void v22Insert(V22InsertVO v22vo) throws Exception {
		v22dao.v22Insert(v22vo);
	}
	@Override
	public void v22Delete(V22InsertVO v22vo) throws Exception {
		v22dao.v22Delete(v22vo);
	}
}
