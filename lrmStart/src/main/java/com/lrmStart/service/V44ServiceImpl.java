package com.lrmStart.service;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lrmStart.dao.V44DAO;
import com.lrmStart.model.V44VO;


@Service("V44Service")
public class V44ServiceImpl implements V44Service {

	    @Resource(name="V44DAO")
	    private V44DAO v44dao;

		@Override
		public void v44ListPrint(V44VO v44vo) throws Exception {
			v44dao.v44ListPrint(v44vo);
		}
	

}
