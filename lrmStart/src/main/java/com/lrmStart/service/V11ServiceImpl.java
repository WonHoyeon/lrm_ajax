package com.lrmStart.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lrmStart.dao.V11DAO;
import com.lrmStart.model.V11InsertModel;
import com.lrmStart.model.V11InsertVO;


@Service("V11Service")
public class V11ServiceImpl implements V11Service {

	    @Resource(name="V11DAO")
	    private V11DAO v11dao;
	
	@Override
	public List<Map<String, Object>> v11Select(String code, String begDate, String endDate) throws Exception {
		return v11dao.v11Select(code, begDate, endDate);
	}
	@Override
	public void v11Insert(V11InsertVO v11vo) throws Exception {
		v11dao.v11Insert(v11vo);
	}
	@Override
	public void v11Delete(V11InsertVO v11vo) throws Exception {
		v11dao.v11Delete(v11vo);
	}
//	@Override
//	public int remove(String no) throws Exception {
//		return v11dao.remove(no); 
//	}
	@Override
	public int remove(V11InsertModel v11mo) throws Exception {
		return v11dao.remove(v11mo); 
	}
}
