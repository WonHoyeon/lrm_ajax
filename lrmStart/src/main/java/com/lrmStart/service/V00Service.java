package com.lrmStart.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.lrmStart.model.V00InsertVO;

public interface V00Service {
	
	public List<Map<String, Object>> v00Select(String hakbun) throws Exception;
	
	public Map<String, Object> stuSelect(String hakbun) throws Exception;
	
	public void v00Insert(V00InsertVO v00vo) throws Exception;
	
	public int isADM(HttpSession session) throws Exception;
	
	public void v00Delete(V00InsertVO v00vo) throws Exception;
	

}