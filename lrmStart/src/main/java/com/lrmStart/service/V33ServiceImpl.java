package com.lrmStart.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lrmStart.dao.V33DAO;
import com.lrmStart.model.V33VO;


@Service("V33Service")
public class V33ServiceImpl implements V33Service {

	    @Resource(name="V33DAO")
	    private V33DAO v33dao;
	
	@Override
	public List<Map<String, Object>> v33Select(String begDate, String endDate) throws Exception {
		return v33dao.v33Select(begDate, endDate);
	}
	@Override
	public void v33Modify(V33VO v33vo) throws Exception {
		v33dao.v33Modify(v33vo);
	}
	@Override
	public void v33Delete(V33VO v33vo) throws Exception {
		v33dao.v33Delete(v33vo);
	}
	@Override
	public void v33CrData() throws Exception {
		v33dao.v33CrData();
	}
	@Override
	public void v33DelData() throws Exception {
		v33dao.v33DelData();
	}
	@Override
	public void v33AllFix(V33VO v33vo) throws Exception {
		v33dao.v33AllFix(v33vo);
	}
}
