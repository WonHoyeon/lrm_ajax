console.log("TestStart");

var vList = (function(){
	
//	function remove(no, callback, error){
//		$.ajax({
//			type : 'put',
//			url : '/lrm/' + no,
////			data : JSON.stringify(data), 
////			contentType:"application/json; charset=utf-8",
//			success: function(result, status, xhr){
//				if (callback){
//					callback(result);
//					console.log(result);
//				}
//			},
//			error : function(xhr, status, er){
//				if(error){
//					error(er);
//				}
//			}
//		})
//	
//	}
	
	
	function remove(data, callback, error){
		$.ajax({
			type : 'put',
			url : '/lrm/' + data.no + '/' + data.userid,
			data : JSON.stringify(data), 
			contentType:"application/json; charset=utf-8",
			success: function(result, status, xhr){
				if (callback){
					callback(result);
					console.log(result);
				}
			},
			error : function(xhr, status, er){
				if(error){
					error(er);
				}
			}
		})
	
	}
	
	
	return {
		remove:remove
	};

	
})();