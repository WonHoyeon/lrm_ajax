<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="java.io.PrintWriter" %>

<head>
	<title>연세대학교 | 도서관 이용수칙 위반 관리 시스템</title>
	<meta charset="UTF-8">
	<meta name="description" content="loans HTML Template">
	<meta name="keywords" content="loans, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	  <!-- Font Awesome -->
  <link rel="stylesheet" href="resources/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="resources/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="resources/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- daterange picker -->
  <link rel="stylesheet" href="resources/plugins/daterangepicker/daterangepicker.css">
    <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="resources/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="resources/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="resources/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="resources/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
</head>

<%
		
			
		
%>
   <body class="hold-transition login-page" onkeydown="javascript:onEnterLogin();">
    <div class="card card-info" style="min-width:400px">
      <div class="card-header">
        <h3 class="card-title" style="text-align: center">도서관 이용수칙 위반 관리 시스템</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form class="form-horizontal" name="loginForm" action="/lrm/main" method="post">
        <div class="card-body">
          <div class="form-group row">
             <div class="col-sm-4">
            <label>ID</label>
            </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="userID" id="userID" placeholder="ID" maxlength="8">
            </div>
          </div>
          <div class="form-group row">
          <div class="col-sm-4">
            <label>Password</label>
            </div>
            <div class="col-sm-8">
              <input type="password" class="form-control" name="userPWD" id="userPWD" placeholder="Password">
            </div>
          </div>
          <div class="form-group row" style="margin-bottom:0">
            <div class="offset-sm-2 col-sm-10">
              <div class="form-check">
                <a style="float: right" class="nav-link" href="" onClick="alert('033-760-2513으로 문의하세요.')">비밀번호를 잊으셨나요?</a>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="button" id="login" class="btn btn-info" style="width:200px"><i class="fas fa-sign-in-alt"></i>&nbsp;로그인</button>
          <button type="reset" class="btn btn-default float-right">취소</button>
        </div>
        <!-- /.card-footer -->
      </form>
    </div>
    <!-- /.card -->
</body>
	<p>- 이 프로그램은 관리자로 권한인증된 기기만 사용 가능합니다.</p>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
if(<%= session.getAttribute("admChk") %> != 1 && document.cookie.match('(^|;) ?' + 'admChk' + '=([^;]*)(;|$)')){
    alert("관리자 로그인후 이용해 주세요.");
}

	$(document).ready(function(){
		<!-- console.log("!????"); -->
		$("#login").click(function(){
			var id = $("#userID").val();
			var pwd = $("#userPWD").val();
			
			if(id ==""){
				alert("ID를 입력하세요.");
				document.form1.title.focus();
				return;
			}
			
			if(pwd == ""){
				alert("비밀번호를 입력하세요.");
				document.form1.content.focus();
				return;
			}
			document.loginForm.submit();			
		})
		

	});
</script>
<script type="text/javascript">

		function onEnterLogin(){
			var keyCode = window.event.keyCode;

			if (keyCode == 13) { //엔테키 이면
				loginForm.submit();
			}
		} //onEnterLogin()
</script>


