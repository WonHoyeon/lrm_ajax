<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<form  method="get" action="v33ListClick">
<div class="row">
	<div class="col-md-6">
	<div class="callout callout-info">
	<!-- Date range -->
                <div class="form-group">
                	<label>기간 선택</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" name="reservation2" id="reservation2" value="${begDate} - ${endDate}">
                    <button type="submit" class="btn btn-secondary">조회</button>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->

			</div>
			</div>
			<div class="col-md-6">
			
			</div>
			</div>
</form>


<div class="row">
	<div class="col-xl-12">
		<div class="card">
			<div class="card card-info">
				<div class="card-header">
				<div class="row">
				<div class="col-2">
					<h3 class="card-title">처벌 확정</h3>
					</div>
					<div class="col-8">
					</div>
					<div class="col-2">
					<a href="/lrm/v33AllFix" class="btn btn-block btn-warning btn-sm" style="color:navy; font-weight:bold">일괄 확정</a>
					</div>
				</div>
			</div>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th style="width:10px">No.</th>
							<th>소속</th>
							<th>학번</th>
							<th>이름</th>
							<th>위반구분</th>
							<th style="width:100px">징계내역</th>
							<th style="width:150px">징계기간</th>
							<th style="width:80px">확정여부</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${vInfo}" var="row" varStatus="status">
	  				
	  				<tr>
	  				<form method="post" action="V33ModifyList" onsubmit="return v33modi()">
								<td>${status.index + 1}</td>
								<td style="width:100px">${row.HKL}</td>
								<td>${row.HAKBUN}</td>
								<td style="min-width:55px">${row.NAME}</td>
								<td>${row.CNAME}</td>
								
								<td> <div class="input-group input-group-sm" style="min-width:200px">
								
								<input class="form-control form-control-navbar" type="number" min="0" id="pcnt" name="pcnt" value="${row.PCNT}" aria-label="Search">
								
								<c:choose>
									<c:when test="${row.PENALTY=='W'}">
								<select class="form-control select2 select2-danger"
												data-dropdown-css-class="select2-danger" name="penalty" id="penalty">
												<option selected="selected" value="W">경고</option>
												<option value="D">일 정지</option>
												<option value="M">달 정지</option>
												<option value="H">학기 정지</option>
												<option value="P">변상</option>
											</select>
									</c:when>
									<c:when test="${row.PENALTY=='D'}">
								<select class="form-control select2 select2-danger"
												data-dropdown-css-class="select2-danger" name="penalty" id="penalty">
												<option selected="selected" value="D">일 정지</option>
												<option value="W">경고</option>
												<option value="M">달 정지</option>
												<option value="H">학기 정지</option>
												<option value="P">변상</option>
											</select>
									</c:when>
									<c:when test="${row.PENALTY=='M'}">
								<select class="form-control select2 select2-danger"
												data-dropdown-css-class="select2-danger" name="penalty" id="penalty">
												<option selected="selected" value="M">달 정지</option>
												<option value="W">경고</option>
												<option value="D">일 정지</option>
												<option value="H">학기 정지</option>
												<option value="P">변상</option>
											</select>
									</c:when>
									<c:when test="${row.PENALTY=='H'}">
								<select class="form-control select2 select2-danger"
												data-dropdown-css-class="select2-danger" name="penalty" id="penalty">
												<option selected="selected" value="H">학기 정지</option>
												<option value="W">경고</option>
												<option value="D">일 정지</option>
												<option value="M">달 정지</option>
												<option value="P">변상</option>
											</select>
									</c:when>
									<c:when test="${row.PENALTY=='P'}">
								<select class="form-control select2 select2-danger"
												data-dropdown-css-class="select2-danger" name="penalty" id="penalty">
												<option selected="selected" value="P">변상</option>
												<option value="W">경고</option>
												<option value="D">일 정지</option>
												<option value="M">달 정지</option>
												<option value="P">변상</option>
											</select>
									</c:when>
									</c:choose>	
									
											</div></td>
								<td><input type="text" class="form-control float-right reservation3" name="reservation3" value="${row.STDATE} - ${row.EDDATE}"></td>
								<td><div class="form-group">
											<c:choose>
											<c:when test="${row.FIX=='Y'}">
											<select class="form-control select2 select2-danger"
												data-dropdown-css-class="select2-danger" name="fix" id="fix">
												<option selected="selected" value="Y">확정</option>
												<option value="N">미확정</option>
											</select>
											</c:when>
											<c:when test="${row.FIX=='N'}">
											<select class="form-control select2 select2-danger"
												data-dropdown-css-class="select2-danger" name="fix" id="fix">
												<option selected="selected" value="N">미확정</option>
												<option value="Y">확정</option>
											</select>
											</c:when>
											</c:choose>
											
											</div></td>
								<td class="text-right py-0 align-middle">
								<div class="row">
								<div class="btn-group btn-group-sm">
											<input type="hidden" name="no" value="${row.NO}"> <button class="btn btn-info btn-sm"><i class="fas fa-pencil-alt"> </i></button>
										</div>
										</form>
								<form method="post" action="V33DeleteList" onsubmit="return v33del()">
									<div class="btn-group btn-group-sm">
											<input type="hidden" name="no" value="${row.NO}"> <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
										</div>
									</div>
									</td>
							</form>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>
			<!-- /.card-body -->
		</div>
		<!-- /.card -->
	</div>
</div>


<div class="row">
	<div class="col-12">
		<div class="card card-info">
			<div class="card-header">
				<h3 class="card-title">위반자 처벌 결정</h3>

				<div class="card-tools">
					<button type="button" class="btn btn-tool"
						data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="card-body p-0" style="margin-top: 1rem">
				<p>
					&nbsp;&nbsp; •위반자의 처벌 수위를 결정합니다. <br />&nbsp;&nbsp; •확정이 완료된 자료에 대해 이용자가 자신의 위반내역을 조회할 수 있습니다. <br />
				</p>
			</div>
			<!-- /.card-body -->
		</div>
		<!-- /.card -->
	</div>
</div>

<script type="text/javascript">
function v33modi() {
	var result = confirm("해당 자료를 수정하시겠습니까?");
	if(result){
	    alert("수정되었습니다.");
	    return true;
	}else {return false};
}
</script>
<script type="text/javascript">
function v33del() {
	var result = confirm("해당 자료를 삭제하시겠습니까?");
	if(result){
	    alert("삭제되었습니다.");
	    return true;
	}else {return false};
}
</script>



<%@ include file="/WEB-INF/views/outline/footer.jsp"%>