<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="java.io.PrintWriter" %>
<%@ include file="/WEB-INF/views/outline/header.jsp" %>


 <div class="callout callout-info">
              <h5><i class="fas fa-book"></i>&nbsp;도서관 이용수칙 위반 관리 시스템</h5>
             -이 프로그램은 관리자로 권한인증된 기기만 사용 가능합니다. </div>

			<% if(userID == null){	%>		
				
			<% } else { 	%>
			<script>
			location.href = 'login.jsp';
			</script>
			<%	}	%>
			
<div class="row">				
  <div class="col-md-6">
   <div class="card" style="height:230px; width: 100%; margin-right:30px">
     <div class="card-header">
       <h3 class="card-title"><i class="fas fa-person-booth"></i>&nbsp;세미나실 예약부도</h3></div>
     <!-- /.card-header -->
     <div class="card-body clearfix">
       <blockquote class="quote-secondary">
         <p>- 세미나실 예약 신청 후 이용하지 않은 경우</p></blockquote></div>
     <!-- /.card-body -->
   </div>
   <!-- /.card -->
    </div>
 <!-- ./col -->
   <div class="col-md-6">
   <div class="card" style="height:230px; width: 100%;">
     <div class="card-header">
       <h3 class="card-title"><i class="fas fa-pizza-slice"></i>&nbsp;음식물 반입</h3></div>
     <!-- /.card-header -->
     <div class="card-body clearfix">
       <blockquote class="quote-secondary">
         <p>- PET병 및 뚜껑이 달린 텀블러 외의 음식물을 반입한 경우<br/>- 지정된 장소 외에서 음식물을 먹은 경우</p></blockquote>
     </div>
     <!-- /.card-body -->
   </div>
   <!-- /.card -->
    </div>
     <!-- ./col -->
        </div>
        <!-- /.row -->
 <div class="row">	
   <div class="col-md-6">
   <div class="card" style="height:230px; width: 100%; margin-right:30px">
     <div class="card-header">
       <h3 class="card-title">
       <i class="fas fa-id-card"></i>&nbsp;출입증 및 계정 대여 </h3></div>
     <!-- /.card-header -->
     <div class="card-body clearfix">
       <blockquote class="quote-secondary">
         <p>- 출입증을 타인에게 대여하거나 타인의 출입증으로 출입한 경우<br/>- 홈페이지 계정을 타인에게 제공한 경우</p></blockquote></div>
     <!-- /.card-body -->
   </div>
   <!-- /.card -->
       </div>
 <!-- ./col -->
   <div class="col-md-6">
   <div class="card" style="height:230px; width: 100%;">
     <div class="card-header">
       <h3 class="card-title">
        <i class="fas fa-times-circle"></i>&nbsp;도서관 부정 이용 </h3></div>
     <!-- /.card-header -->
     <div class="card-body clearfix">
       <blockquote class="quote-secondary">
         <p>- 관리자의 허락이 없는 상태에서 출입증 접촉 없이 출입한 경우<br/>
						- 들어온 기록 없이 외부인 자격으로 나가려고 한 경우<br/>
						- 도서관 이용 금지 기간 중 출입한 경우<br/>
						- 도서관 자료 및 시설물 등을 승인 없이 이용한 경우</p></blockquote></div>
     <!-- /.card-body -->
   </div>
   <!-- /.card -->
       </div>
 <!-- ./col -->
       </div>
        <!-- /.row -->
 <div class="col-lg-6">
	<a type="button" href="resources/file/LRM.pdf" 
		target="_blank" class="btn btn-block btn-info btn-lg"><i class="fas fa-download"></i>관리자 설명서 다운로드</a>
</div>


<%@ include file="/WEB-INF/views/outline/footer.jsp" %>
