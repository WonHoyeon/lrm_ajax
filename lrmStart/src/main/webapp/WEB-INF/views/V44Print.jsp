<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>

<div class="row">
<div class="col-12">
	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title">처리내역 출력</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool"
					data-card-widget="collapse" data-toggle="tooltip"
					title="Collapse">
					<i class="fas fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="card-body p-0" style="margin-top:1rem">
			<p>&nbsp;&nbsp; •출력할 기간을 설정하고 출력버튼을 누르십시오. <br /> &nbsp;&nbsp; •기간별
							위반자 처리내역을 출력(또는 파일저장) 할 수 있습니다. <br /> &nbsp;&nbsp; •조회기간은
							처리기준일자입니다. <br/>
			</p>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>
<!-- /.col -->
</div>


<div class="row">
<div class="col-md-4">
<div class="callout callout-info">
	<div class="form-group">
	<label>위반구분</label>
	<select class="form-control select2 select2-danger"
		data-dropdown-css-class="select2-danger" name="code" id="code">
		<option selected="selected" value="A">전체</option>
		<option value="R05">세미나실 예약부도</option>
		<option value="R07">음식물 반입</option>
		<option value="R03">출입증 및 계정 대여</option>
		<option value="R04">도서관 부정 이용</option>
		<option value="R11">인쇄물 배포 및 물품 판매</option>
		<option value="R01">훼손 및 무단반출</option>
		<option value="R08">흡연</option>
	</select>
	</div>
	</div>
	</div>

	
<div class="col-md-4">
<div class="callout callout-info">
	<div class="form-group">
	<label>확정구분</label>
	<select class="form-control select2 select2-danger"
		data-dropdown-css-class="select2-danger" name="fix" id="fix">
		<option selected="selected" value="A">전체</option>
		<option value="N">미확정</option>
		<option value="Y">확정</option>
	</select>
	</div>
	</div>
	</div>
	
	<div class="col-md-4">
	<div class="callout callout-info">
	<!-- Date range -->
       <div class="form-group">
       	<label>조회기간</label>
         <div class="input-group">
           <div class="input-group-prepend">
             <span class="input-group-text">
               <i class="far fa-calendar-alt"></i>
             </span>
           </div>
           <input type="text" class="form-control float-right" name="reservation2" id="reservation2">
         </div>
         <!-- /.input group -->
       </div>
       <!-- /.form group -->
</div>
</div>
</div>

<div class="row" style="margin-top:50px; margin-bottom:50px">
 <div class="col-lg-3">
 </div>
 <div class="col-lg-6">
	<button id="v44print"
		class="btn btn-block btn-warning btn-lg"><i class="fas fa-print"></i>&nbsp;&nbsp;출력 및 저장</button>
</div>
 <div class="col-lg-3">
 </div>
</div>



        <script src="https://rd.yonsei.ac.kr/ReportingServer/html5/js/jquery-1.11.0.min.js"></script>
        <script src="https://rd.yonsei.ac.kr/ReportingServer/html5/js/crownix-viewer.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://rd.yonsei.ac.kr/ReportingServer/html5/css/crownix-viewer.min.css">

<script type="text/javascript">
$(document).ready(function(){
	$("#v44print").click(function(){
		
window.open('/lrm/V44ListPrint?code='+$("#code").val()+'&reservation2='+$("#reservation2").val()+'&fix='+$("#fix").val()+'&toolbar=Y&zoom=0','print','width=2000,height=1000,top=0,scrollbars=no');
})
});
</script>

<%@ include file="/WEB-INF/views/outline/footer.jsp"%>