<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>

<form  method="get" action="V11ListClick">
<div class="row">
<div class="col-md-6">
<div class="callout callout-info">
	<div class="form-group">
	<label>위반구분</label>
	<select class="form-control select2 select2-danger" name="code"
		data-dropdown-css-class="select2-danger">
		<option selected="selected" value="R05">세미나실 예약부도</option>
		<option value="R07">음식물 반입</option>
		<option value="R03">출입증 및 계정 대여</option>
		<option value="R04">도서관 부정 이용</option>
		<option value="R11">인쇄물 배포 및 물품 판매</option>
		<option value="R01">훼손 및 무단반출</option>
		<option value="R08">흡연</option>
	</select>
	</div>
	</div>
	</div>
	<div class="col-md-6">
	<div class="callout callout-info">
	<!-- Date range -->
                <div class="form-group">
                	<label>조회기간</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" name="reservation2" id="reservation2">
                    <button type="submit" class="btn btn-secondary">조회</button>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->

			</div>
			</div>
			</div>


</form>
	
	
<div class="row">
<div class="col-12">
	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title">위반자 입력(구분별)</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool"
					data-card-widget="collapse" data-toggle="tooltip"
					title="Collapse">
					<i class="fas fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="card-body p-0" style="margin-top:1rem">
			<p>&nbsp;&nbsp; •위반구분별로 위반자 입력이 가능합니다. <br />
							&nbsp;&nbsp; •기간을 설정하면 해당기간에 대한 위반사항이 표시됩니다. <br />
			</p>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>
<!-- /.col -->
</div>



<%@ include file="/WEB-INF/views/outline/footer.jsp"%>