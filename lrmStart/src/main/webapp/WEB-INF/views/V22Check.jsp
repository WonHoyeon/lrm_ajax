<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<div class="row">
<div class="col-6">
	<div class="callout callout-info">
		<div class="form-group">
	<label>이름(일부만 입력 가능)</label>
		<form class="form-horizontal" method="get" action="V22ListClick">
			<div class="input-group input-group-sm mb-0">
				<input class="form-control form-control-sm"
					placeholder="이름" name="name" maxlength="10">
				<div class="input-group-append">
					<button type="submit" class="btn btn-secondary">조회</button>
				</div>
				</div>
			</form>
			</div>
	</div>
</div>
</div>


<div class="row">
	<div class="col-12">
	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title">위반자 입력(외부인)</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool"
					data-card-widget="collapse" data-toggle="tooltip"
					title="Collapse">
					<i class="fas fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="card-body p-0" style="margin-top:1rem">
			<p>&nbsp;&nbsp; •본교 학생이 아닌 외부이용자들을 등록하는 메뉴입니다. <br /> &nbsp;&nbsp;
							•중복확인 후 등록되지 않은 자료만 입력하기 바랍니다. <br /> &nbsp;&nbsp;
			</p>
			</div>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>
<!-- /.col -->



<%@ include file="/WEB-INF/views/outline/footer.jsp"%>