<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<section class="content">
	<div class="container-fluid">
<div class="row">
<div class="col-6">
	<div class="callout callout-info">
		<div class="form-group">
	<label>학번</label>
		<form class="form-horizontal" method="get" action="V00ListClick">
			<div class="input-group input-group-sm mb-0">
				<input class="form-control form-control-sm"
					value="${hakbun}" name="hakbun" maxlength="10">
				<div class="input-group-append">
					<button type="submit" class="btn btn-secondary">조회</button>
				</div>
				</div>
			</form>
			</div>
	</div>
</div>
</div>
<div class="row">
<div class="col-12">
				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">이용수칙 위반내역 :&nbsp;&nbsp;${stuInfo.HAKKWA}&nbsp;&nbsp;${stuInfo.NAME}&nbsp;&nbsp;${stuInfo.JAEHAK}</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>위반일자</th>
									<th>위반구분</th>
									<th>위반장소</th>
									<th>위반내역</th>
									<th>입력일자</th>
									<th>입력자</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
	                			<c:forEach items="${vInfo}" var = "row" varStatus = "status">		
								<tr>
									<td>${status.index + 1}</td>									
									<td>${fn:substring(row.PDATE,2,10)}</td>
									<td>${row.CODE}</td>
									<td>${row.PLACE}</td>
									<td>${row.BIGO}</td>
									<td>${fn:substring(row.IDATE,2,10)}</td>
									<td>${row.IADM}</td>
									<td class="text-right py-0 align-middle">
									
									
									<form method="post" action="V00DeleteList" onsubmit="return v00del()">
										<div class="btn-group btn-group-sm">
											<input type="hidden" name="no" value="${row.NO}"> <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
										</div>
										</form>
										
										
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
				</div>
				</div>
<div class="row">
<div class="col-12">
				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">추가입력</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body" onkeydown="javascript:onEnterInsert();">
					  <form name="v00form" action="/lrm/V00InsertList" method="get">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>위반일자</th> 
									<th>위반구분</th> 
									<th>위반장소</th>
									<th>위반내역</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr> 
									<td>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"> <i
													class="far fa-calendar-alt"></i>
												</span>
											</div>
											<input type="text" class="form-control float-right" name="reservation" id="reservation">
										</div>
									</td>
									<td><select class="form-control select2 select2-danger"
										data-dropdown-css-class="select2-danger" name="code" id="code" style="width: 100%;">
											<option selected="selected" value='R05'>세미나실 예약부도</option>
											<option value='R07'>음식물 반입</option>
											<option value='R03'>출입증 및 계정 대여</option>
											<option value='R04'>도서관 부정 이용</option>
											<option value='R11'>인쇄물 배포 및 물품 판매</option>
											<option value='R01'>훼손 및 무단반출</option>
											<option value='R08'>흡연</option>
									</select></td>
									<td><div class="form-group">
											<input	type="text" class="form-control" name="place" id="place">
										</div></td>
									<td><div class="form-group">
											<input type="text" class="form-control" name="details" id="details">
										</div></td>
									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<button type="button" id="v00insert" class="btn btn-info"><i class="fas fa-plus"></i></button>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					  </form> 
					</div>
					<!-- /.card-body -->
				</div>
</div>
</div>


<div class="row">
<div class="col-12">
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">위반자 입력(개인별)</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool"
								data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body p-0" style="margin-top:1rem">
						<p>&nbsp;&nbsp; •개인별 위반내역 입력이 가능합니다. <br />&nbsp;&nbsp; •학번
							또는 이용자ID를 입력하고 검색을 누르면 위반내역을 입력할 수 있습니다. <br />
						</p>

					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
		</div>
		<!-- /.col -->
		</div>
	<!-- /.row -->
</section>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		<!-- console.log("!????"); -->
		$("#v00insert").click(function(){
			var reservation = $("#reservation").val();
			var code = $("#code").val();
			var place = $("#place").val();
			var details = $("#details").val();
			if(reservation ==""){
				alert("위반일자를 입력해 주세요.");
				document.v00form.reservation.focus();
				return;
			}
			if(code == ""){
				alert("위반구분을 입력해 주세요.");
				document.v00form.vGubun.focus();
				return;
			}
			if(place ==""){
				alert("위반장소를 입력해 주세요.");
				document.v00form.place.focus();
				return;
			}
			if(details ==""){
				alert("위반내역을 입력해 주세요.");
				document.v00form.details.focus();
				return;
			}
			document.v00form.submit();			
		});
	});
</script>
<script type="text/javascript">
		function onEnterInsert(){
			var keyCode = window.event.keyCode;

			if (keyCode == 13) { //엔테키 이면
				v00insert.click();
			}
		} //onEnterLogin()
</script>

<script type="text/javascript">
function v00del() {
	var result = confirm("해당 자료를 삭제하시겠습니까?");
	if(result){
	    alert("삭제되었습니다.");
	    return true;
	}else {return false};
}
</script>
<%@ include file="/WEB-INF/views/outline/footer.jsp"%>