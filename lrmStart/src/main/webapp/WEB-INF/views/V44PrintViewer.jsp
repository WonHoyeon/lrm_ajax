<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html style="margin:0;height:100%">
    <head>
        <script src="https://rd.yonsei.ac.kr/ReportingServer/html5/js/jquery-1.11.0.min.js"></script>
        <script src="https://rd.yonsei.ac.kr/ReportingServer/html5/js/crownix-viewer.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://rd.yonsei.ac.kr/ReportingServer/html5/css/crownix-viewer.min.css">
    </head>
    <body>
        <div id="crownix-viewer" style="position:absolute;width:100%;height:100%"></div>
        <script type="text/javascript">

         window.onload = function(){
                var viewer = new m2soft.crownix.Viewer('https://rd.yonsei.ac.kr/ReportingServer/service', 'crownix-viewer');
				
					/* viewer.hideToolbarItem(["save"]); */
					 viewer.ZoomRatio = 55; 
					 viewer.AutoAdjust = false;
					viewer.openFile('${report_name}.mrd', '/rp [${code}] [${begDate}] [${endDate}] [${fix}]', {timeout:180});
            };
        
        </script>
    </body>
</html>