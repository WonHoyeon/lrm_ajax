<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<form  method="get" action="V11ListClick">
<div class="row">
<div class="col-md-6">
<div class="callout callout-info">
	<div class="form-group">
	<label>위반구분</label>
	<select class="form-control select2 select2-danger" name="code"
		data-dropdown-css-class="select2-danger">
		<option selected="selected" value="R05">세미나실 예약부도</option>
		<option value="R07">음식물 반입</option>
		<option value="R03">출입증 및 계정 대여</option>
		<option value="R04">도서관 부정 이용</option>
		<option value="R11">인쇄물 배포 및 물품 판매</option>
		<option value="R01">훼손 및 무단반출</option>
		<option value="R08">흡연</option>
	</select>
	</div>
	</div>
	</div>
	<div class="col-md-6">
	<div class="callout callout-info">
	<!-- Date range -->
                <div class="form-group">
                	<label>조회기간</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" name="reservation2" id="reservation2" value="${begDate} - ${endDate}">
                    <button type="submit" class="btn btn-secondary">조회</button>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->

			</div>
			</div>
			</div>


</form>
			
			
<div class="row">
<div class="col-12">
				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">${listInfo}&nbsp;위반내역</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>위반일자</th>
									<th>학번</th>
									<th>위반장소</th>
									<th>위반내역</th>
									<th>입력일자</th>
									<th>입력자</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
	                			<c:forEach items="${vInfo}" var = "row" varStatus = "status">		
								<tr>
									<td>${status.index + 1}</td>									
									<td>${fn:substring(row.PDATE,2,10)}</td>
									<td>${row.HAKBUN}</td>
									<td>${row.PLACE}</td>
									<td>${row.BIGO}</td>
									<td>${fn:substring(row.IDATE,2,10)}</td>
									<td>${row.IADM}</td>
								<!--	 <td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="/lrm/V11DeleteList?No=${row.NO}&Id=${row.IADM}" 
											class="btn btn-danger"><i
												class="fas fa-trash"></i></a>
										</div>
									</td> -->
									
									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
										
										
											<form method="post" action="V11DeleteList" onsubmit="return v11del()">
										<div class="btn-group btn-group-sm">
											<input type="hidden" name="no" value="${row.NO}"> <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
										</div>
										</form>
												
												
										</div>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
				</div>
				</div>
<div class="row">
<div class="col-12">
				<div class="card">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">추가입력</h3>
						</div>
					</div>
					<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
					<!-- /.card-header -->
					<div class="card-body" onkeydown="javascript:onEnterInsert();">
					  <form name="v11form" action="/lrm/V11InsertList" method="get">
						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>위반일자</th> 
									<th>학번</th> 
									<th>위반장소</th>
									<th>위반내역</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr> 
									<td>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text"> <i
													class="far fa-calendar-alt"></i>
												</span>
											</div>
											<input type="text" class="form-control float-right" name="reservation" id="reservation">
										</div>
									</td>
									<td><div class="form-group">
											<input	type="text" class="form-control" name="hakbun" id="hakbun" maxlength="10">
										</div></td>
									<td><div class="form-group">
											<input	type="text" class="form-control" name="place" id="place">
										</div></td>
									<td><div class="form-group">
											<input type="text" class="form-control" name="details" id="details">
										</div></td>
									<td class="text-right py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<button type="button" id="v11insert" class="btn btn-info"><i class="fas fa-plus"></i></button>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					  </form> 
					</div>
					<!-- /.card-body -->
				</div>
</div>
</div>
<div class="row">
<div class="col-12">
				<div class="card card-info">
					<div class="card-header">
						<h3 class="card-title">위반자 입력(개인별)</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool"
								data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body p-0" style="margin-top:1rem">
						<p>&nbsp;&nbsp; •위반구분별로 위반자 입력이 가능합니다. <br />
							&nbsp;&nbsp; •기간을 설정하면 해당기간에 대한 위반사항이 표시됩니다. <br />
			</p>

					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
		</div>


<script type="text/javascript">
	$(document).ready(function(){
	
		$(".btn-danger").each(function(i){
			$(this).click(function(e){
				
				var list1 = $(this).attr('value');
				var list = list1.split(',');
				var listNo = list[0];
				var listIadm = list[1];
				console.log(listNo);
				console.log(listIadm);
				
				vList.remove({no: listNo, userid: listIadm}, function(result){
					if(result === "success"){
						alert("EPEPEPEP");
					}else{
						alert("뺴");
					}
				
				})
				
			});
		});
	
		$("#v11insert").click(function(){
			var reservation = $("#reservation").val();
			var hakbun = $("#hakbun").val();
			var place = $("#place").val();
			var details = $("#details").val();
			
			if(reservation ==""){
				alert("위반일자를 입력해 주세요.");
				document.v11form.reservation.focus();
				return;
			}
			if(hakbun == ""){
				alert("학번을 입력해 주세요.");
				document.v11form.vGubun.focus();
				return;
			}
			if(place ==""){
				alert("위반장소를 입력해 주세요.");
				document.v11form.place.focus();
				return;
			}
			if(details ==""){
				alert("위반내역을 입력해 주세요.");
				document.v11form.details.focus();
				return;
			}
			document.v11form.submit();			
		});
	
		
	});
</script>
<script type="text/javascript">

		function onEnterInsert(){
			var keyCode = window.event.keyCode;

			if (keyCode == 13) { //엔테키 이면
				v11insert.click();
			
			
	
			}
		} //onEnterLogin()
</script>

<script type="text/javascript">
function v11del() {
	var result = confirm("해당 자료를 삭제하시겠습니까?");
	if(result){
	    alert("삭제되었습니다.");
	    return true;
	}else {return false};
}
</script>
<%@ include file="/WEB-INF/views/outline/footer.jsp"%>