<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<div class="row">
	<div class="col-6">
		<div class="callout callout-info">
			<div class="form-group">
				<label>이름(일부만 입력 가능)</label>
				<form class="form-horizontal" method="get" action="V22ListClick">
					<div class="input-group input-group-sm mb-0">
						<input class="form-control form-control-sm" placeholder="이름"
							name="name" maxlength="10">
						<div class="input-group-append">
							<button type="submit" class="btn btn-secondary">조회</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title">외부인 위반내역</h3>
				</div>
			</div>
			<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
			<!-- /.card-header -->
			<div class="card-body">
				<table id="example2" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>No.</th>
							<th>위반일자</th>
							<th>거주지</th>
							<th>이름</th>
							<th>연락처</th>
							<th>비고</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${vInfo}" var="row" varStatus="status">
							<tr>
								<td>${status.index + 1}</td>
								<td>${row.PDATE}</td>
								<td>${row.REGION}</td>
								<td>${row.NAME}</td>
								<td>${row.HTELNO}</td>
								<td>${row.BIGO}</td>
								<td class="text-right py-0 align-middle">
								
								
								<form method="post" action="V22DeleteList" onsubmit="return v22del()">
										<div class="btn-group btn-group-sm">
											<input type="hidden" name="no" value="${row.NO}"> <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
										</div>
										</form>
									
									
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<!-- /.card-body -->
		</div>
		<!-- /.card -->
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title">추가입력</h3>
				</div>
			</div>
			<!--  		<div class="card-header">
						<h4 class="card-title">이용수칙 위반내역 :</h4>
					</div>-->
			<!-- /.card-header -->
			<div class="card-body" onkeydown="javascript:onEnterInsert();">
				<form name="v22form" action="/lrm/V22InsertList" method="GET">
					<table id="example2" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>위반일자</th>
								<th>거주지</th>
								<th>이름</th>
								<th>연락처</th>
								<th>비고</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"> <i
												class="far fa-calendar-alt"></i>
											</span>
										</div>
										<input type="text" class="form-control float-right"
											name="reservation" id="reservation">
									</div>
								</td>
								<td><div class="form-group">
										<input type="text" class="form-control" name="region"
											id="region">
									</div></td>
								<td><div class="form-group">
										<input type="text" class="form-control" name="name" id="name">
									</div></td>
								<td>
								<div class="input-group">
								<div class="input-group-prepend">
										<span class="input-group-text"><i class="fas fa-phone"></i></span>
									</div> <input type="text" class="form-control" name="htelno"
									id="htelno"
									data-inputmask="'mask': ['999-9999-9999']"
									data-mask></div>
								</td>
								<td><div class="form-group">
										<input type="text" class="form-control" name="bigo" id="bigo">
									</div></td>
								<td class="text-right py-0 align-middle">
									<div class="btn-group btn-group-sm">
										<button type="button" id="v22insert" class="btn btn-info">
											<i class="fas fa-plus"></i>
										</button>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
			<!-- /.card-body -->
		</div>
	</div>
</div>

<div class="row">
	<div class="col-12">
		<div class="card card-info">
			<div class="card-header">
				<h3 class="card-title">위반자 입력(외부인)</h3>

				<div class="card-tools">
					<button type="button" class="btn btn-tool"
						data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="card-body p-0" style="margin-top: 1rem">
				<p>&nbsp;&nbsp; •본교 학생이 아닌 외부이용자들을 등록하는 메뉴입니다. <br /> &nbsp;&nbsp;
							•중복확인 후 등록되지 않은 자료만 입력하기 바랍니다. <br /> &nbsp;&nbsp;
			</p>
			</div>
			<!-- /.card-body -->
		</div>
		<!-- /.card -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		<!-- console.log("!????"); -->
		$("#v22insert").click(function(){
			var reservation = $("#reservation").val();
			var region = $("#region").val();
			var name = $("#name").val();
			var telno = $("#telno").val();
			var bigo = $("#bigo").val();
			if(reservation ==""){
				alert("위반일자를 입력해 주세요.");
				document.v22form.reservation.focus();
				return;
			}
			if(region == ""){
				alert("거주지를 입력해 주세요.");
				document.v22form.vGubun.focus();
				return;
			}
			if(name ==""){
				alert("이름을 입력해 주세요.");
				document.v22form.place.focus();
				return;
			}
			if(telno ==""){
				alert("연락처를 입력해 주세요.");
				document.v22form.details.focus();
				return;
			}
			if(bigo ==""){
				alert("비고를 입력해 주세요.");
				document.v22form.details.focus();
				return;
			}
			document.v22form.submit();			
		})
	});
</script>
<script type="text/javascript">

		function onEnterInsert(){
			var keyCode = window.event.keyCode;

			if (keyCode == 13) { //엔테키 이면
				v22insert.click();
			}
		} //onEnterLogin()
</script>


<script type="text/javascript">
function v22del() {
	var result = confirm("해당 자료를 삭제하시겠습니까?");
	if(result){
	    alert("삭제되었습니다.");
	    return true;
	}else {return false};
}
</script>


<%@ include file="/WEB-INF/views/outline/footer.jsp"%>