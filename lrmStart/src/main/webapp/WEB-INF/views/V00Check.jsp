<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<div class="row">
<div class="col-6">
	<div class="callout callout-info">
		<div class="form-group">
	<label>학번</label>
		<form class="form-horizontal" method="get" action="V00ListClick">
			<div class="input-group input-group-sm mb-0">
				<input class="form-control form-control-sm"
					placeholder="학번 또는 ID" name="hakbun" maxlength="10">
				<div class="input-group-append">
					<button type="submit" class="btn btn-secondary">조회</button>
				</div>
				</div>
				</form>
			</div>
	</div>
</div>
</div>


<div class="row">
	<div class="col-12">
	<div class="card card-info">
		<div class="card-header">
			<h3 class="card-title">위반자 입력(개인별)</h3>

			<div class="card-tools">
				<button type="button" class="btn btn-tool"
					data-card-widget="collapse" data-toggle="tooltip"
					title="Collapse">
					<i class="fas fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="card-body p-0" style="margin-top:1rem">
			<p>&nbsp;&nbsp; •개인별 위반내역 입력이 가능합니다. <br />&nbsp;&nbsp; •학번
				또는 이용자ID를 입력하고 검색을 누르면 위반내역을 입력할 수 있습니다. <br />
			</p>
			</div>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>
<!-- /.col -->



<%@ include file="/WEB-INF/views/outline/footer.jsp"%>