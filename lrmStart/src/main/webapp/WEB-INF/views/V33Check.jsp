<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/outline/header.jsp"%>


<div class="row" style="margin-top:50px">
	<div class="col-md-3">
	</div>
		<div class="col-md-7">
<!-- Profile Image -->
<form  method="post" action="V33Data">
            <div class="card card-info card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="resources/img/scales-152494_1280.png"
                       alt="">
                </div>

                <h3 class="profile-username text-center">처벌 자료 생성</h3>

                <p class="text-muted text-center">위반자별로 입력된 자료를 총 합산하여 처벌 기준 자료를 생성합니다.</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>세미나실 예약부도 3회 이상 위반</b> <a class="float-right">14일간 자리배정 및 세미나실 이용 정지</a>
                  </li>
                  <li class="list-group-item">
                    <b>음식물 반입 3회 이상 위반</b> <a class="float-right">1개월간 도서관 이용 정지, 학과장 지도 의뢰</a>
                  </li>
                  <li class="list-group-item">
                    <b>출입증 및 계정 대여 2회 이상 위반</b> <a class="float-right">1학기동안 도서관 이용 정지</a>
                  </li>
                  <li class="list-group-item">
                    <b>이용시설 훼손</b> <a class="float-right">수리비용 청구</a>
                  </li>
                  <li class="list-group-item">
                    <b>인쇄물 배포 및 물품의 판매</b> <a class="float-right">퇴실조치 및 원상회복 지시</a>
                  </li>
                </ul>
               <input type="hidden" class="form-control float-right" name="reservation2" id="reservation2">

                <button type="submit" class="btn btn-info btn-block"><b>자료 확인</b></button>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </form>
            </div>
      	<div class="col-md-2">
      	</div>
      	</div>




<%@ include file="/WEB-INF/views/outline/footer.jsp"%>