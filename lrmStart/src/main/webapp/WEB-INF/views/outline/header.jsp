<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>

<html lang="en">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<head>
	<title>연세대학교 | 도서관 이용수칙 위반 관리 시스템</title>
	<meta charset="UTF-8">
	<meta name="description" content="loans HTML Template">
	<meta name="keywords" content="loans, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	
	<link rel="icon" href="resources/img/favicon.ico" />
	  <!-- Font Awesome -->
  <link rel="stylesheet" href="resources/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="resources/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="resources/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="resources/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- daterange picker -->
  <link rel="stylesheet" href="resources/plugins/daterangepicker/daterangepicker.css">
    <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="resources/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="resources/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="resources/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="resources/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

</head>
		<%
			String userID = null;
		if(session.getAttribute("userID") != null){
			userID = (String)session.getAttribute("userID");   //로그인 된 사용자 기억하기
		}
		%>
<body class="hold-transition layout-top-nav">
<div class="wrapper">
    <!-- Navbar -->
<nav class="main-header navbar navbar-expand-xl navbar-light navbar-white">
    <div class="container">
      
      <a href="${path}/lrm/main" class="navbar-brand" >
    <img src="resources/img/logo.png" alt="도서관 이용수칙 위반 관리 시스템" class="brand-image img-circle elevation-3"
             style="opacity: .8">
         <span class="brand-text" style="font-weight: 900; font-family: yonsei">도서관 이용수칙 위반 관리 시스템</span></a>
 <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" 
 		aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="V00Check" class="nav-link">위반자 입력(개인별)</a>
          </li>
          <li class="nav-item">
            <a href="V11Check" class="nav-link">위반자 입력(구분별)</a>
          </li>
          <li class="nav-item">
            <a href="V22Check" class="nav-link">위반자 입력(외부인)</a>
          </li>
          <li class="nav-item">
            <a href="V33Check" class="nav-link">처벌 결정</a>
          </li>
          <li class="nav-item">
            <a href="V44Print" class="nav-link">출력</a>
          </li>
          <li class="nav-item" style="position:absolute ; right:0; margin-right: 50px">
            <a href="logout" class="nav-link">로그아웃&nbsp;<i class="fas fa-sign-out-alt"></i></a>
          </li>
       </ul>
    </div>
    </div>
  </nav>
  <!-- /.navbar -->


  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container" style="max-width: 1300px">
  
  
  
  


    










