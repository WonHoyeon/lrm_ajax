<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


  </div>
	  </div>
	  </div>
	
	  <!-- Main Footer -->
 <center> <footer class="main-footer">
    <!-- Default to the left -->
    Copyright &copy; &nbsp; 2020 YONSEI UNIVERSITY MIRAE CAMPUS.&nbsp; 
			     		 &nbsp; ALL RIGHTS RESERVED.&nbsp;&nbsp;&nbsp; (220-710) 강원도 원주시 연세대길1  
			      <a href="https://www.yonsei.ac.kr/wj" target="_blank">연세대학교 미래캠퍼스</a>
  </footer></center>
  </div>


	
	<!--====== Javascripts & Jquery ======-->
<script src="resources/js/jquery-3.2.1.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/jquery.slicknav.min.js"></script>
<script src="resources/js/owl.carousel.min.js"></script>
<script src="resources/js/jquery-ui.min.js"></script>
<script src="resources/js/main.js"></script>
	
	<!-- jQuery -->
<script src="resources/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="resources/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="resources/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="resources/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="resources/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="resources//dist/js/demo.js"></script>
<!-- page script -->
<script src="resources/plugins/moment/moment.min.js"></script>
<script src="resources/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="resources/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->


<script type="text/javascript">
  $(function () {
	  
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
       
    });
        //Date range picker
    $('#reservation').daterangepicker({
    	"singleDatePicker": true,
    	locale:{"format": "YYYY-MM-DD"},
    	
    });
    //Date range picker
    $('#reservation2').daterangepicker({
    	locale:{"format": "YYYY-MM-DD"},
    	
    		startDate: moment().subtract(7, 'days'),
            endDate: moment().add(1, 'days')
    	
    });

    $('.reservation3').daterangepicker({
 	   locale:{"format": "YYYYMMDD"},
 	});
    //Money Euro
    $('[data-mask]').inputmask()
        
   });

</script>
</body>
</html>